import tensorflow as tf
from fastapi import FastAPI, UploadFile
from fastapi.responses import HTMLResponse
import uvicorn, base64
import numpy as np
import cv2

app = FastAPI()

#Load model
model = tf.keras.models.load_model('model.h5')

ara_alpha_numeric = {
    'ا': 0, 'ب': 1, 'ت': 2, 'ث': 3, 'ج': 4, 'ح': 5, 'خ': 6, 'د': 7, 'ذ': 8, 'ر': 9,
      'ز': 10, 'س': 11, 'ش': 12, 'ص': 13, 'ض': 14, 'ط': 15, 'ظ': 16, 'ع': 17,
        'غ': 18, 'ف': 19, 'ق': 20, 'ك': 21, 'ل': 22, 'لا': 23, 'م': 24, 'ن': 25,
          'ه': 26, 'و': 27, 'ي': 28, '٠': 29, '١': 30, '٢': 31, '٣': 32, '٤': 33,
            '٥': 34, '٦': 35, '٧': 36, '٨': 37, '٩': 38
    }

def ocr(content):
    img_rows , img_cols = 28,28
    # Convert image to numpy array
    bytes = bytearray(content)
    numpy_img = np.asarray(bytes, dtype=np.uint8)
    img = cv2.imdecode(numpy_img,cv2.IMREAD_GRAYSCALE)
    # Resize and Reshape
    img = cv2.resize(img, dsize=(img_rows,img_cols), interpolation=cv2.INTER_CUBIC)
    img = img.reshape(1, img_rows, img_cols, 1).astype('float32')
    img /= 255
    # Predict using loaded model
    prediction = model.predict(img)
    return list(ara_alpha_numeric.keys())[list(ara_alpha_numeric.values()).index(prediction.argmax(axis=-1)[0])], prediction.max(axis=-1)[0]

@app.get("/",response_class=HTMLResponse)
async def root():
    return """
    <html>
        <head>
            <title>Upload File</title>
        </head>
        <body>
            <form action="/uploadfile/" enctype="multipart/form-data" method="post">
                <input name="file" type="file">
                <input type="submit">   
            </form>
        </body>
    """

@app.post("/uploadfile/",response_class=HTMLResponse)
async def create_upload_file(file: UploadFile):
    # Read file
    content = file.file.read()
    # First pass to OCR
    result = ocr(content)
    return f"""
    <html>
        <head>
            <title>Uploaded File</title>
        </head>
        <body>
            <p> The letter is: {result[0]}</p>
            <p> The probability is: {result[1]}</p>
            <img src="data:image/png;base64,{base64.b64encode(content).decode('ascii')}"/>
        </body>
    </html>
    """

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000)