## Name
Arabic OCR

## Description
A simple API that takes as input, a single image of an arabic letter, and returns as output a prediction of the letter along with a probability.

## Badges
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/medoalmasry/arabic-ocr)](https://gitlab.com/medoalmasry/arabic-ocr/-/commits/main)
[![GitLab all issues](https://img.shields.io/gitlab/issues/all/medoalmasry/arabic-ocr)](https://gitlab.com/medoalmasry/arabic-ocr/-/issues)
[![GitLab forks](https://img.shields.io/gitlab/forks/medoalmasry/arabic-ocr)](https://gitlab.com/medoalmasry/arabic-ocr/-/forks)

## Visuals
![Visual Demo](https://gitlab.com/medoalmasry/arabic-ocr/-/raw/337f66c52248081da8da854f90189b032ac5f057/resources/demo.gif)

## Installation
```
git clone git@gitlab.com:medoalmasry/arabic-ocr.git
cd arabic-ocr
docker build -t arabic-ocr .
docker run -p 127.0.0.1:8000:8000 arabic-ocr
```
Open the [localhost url](http://127.0.0.1:8000) from local browser

## Dataset
<details>
<summary><b>Arabic Letters Numbers OCR Dataset</b></summary>
[Download](https://www.kaggle.com/datasets/mahmoudreda55/arabic-letters-numbers-ocr/download?datasetVersionNumber=4)

Dataset consists of 29 letters and 10 digits.

```shell
-- Dataset
| --  ١    
|     | -- 0.png
|     | -- 1.png
|     | -- ...
| --  ٢
|     | -- 0.png
|     | -- 1.png
|     | -- ...
| --  س
|     | -- 0.png
|     | -- 1.png
|     | -- ...
...
```
</details>

## Training
* Install requirements
```
git clone git@gitlab.com:medoalmasry/arabic-ocr.git
cd arabic-ocr
pip install -U --user -r requirements.txt
```
* Edit model in [notebook](https://gitlab.com/medoalmasry/arabic-ocr/-/blob/main/ocr.ipynb)

## Authors & acknowledgment
<a href="https://gitlab.com/medoalmasry"><img src="https://gitlab.com/uploads/-/system/user/avatar/13834902/avatar.png?width=64"></a>
<a href="https://github.com/arkrow"><img width="64" height="64" src="https://avatars.githubusercontent.com/u/9433802"></a>

I would like to thank [Hazem Nabil](https://github.com/arkrow) for his help with debugging docker's build errors.

## License
BSD 3-Clause License 

## Project status
CLOSED
